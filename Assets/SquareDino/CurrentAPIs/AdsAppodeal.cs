//using System;
//using System.Collections;
//using AppodealAds.Unity.Api;
//using AppodealAds.Unity.Common;
//using UnityEngine;
//using Facebook.Unity;
//
//public class AdsAppodeal : MonoBehaviour, IInterstitialAdListener, IRewardedVideoAdListener, IBannerAdListener, IPermissionGrantedListener 
//{
//    #if UNITY_ANDROID
//        private const string AppKey = "";
//    #elif UNITY_IPHONE
//        private const string AppKey = "ecbec2d36af4cab3e4ec47941621e1c6a18d2a2ca03d3d63";
//    #else
//		private const string AppKey = "";
//	#endif
//    
//    public bool testingMode;
//    public bool loggingMode;
//    private bool _isInterstitialReady;
//    private bool _isRewardedLoaded;
//    private bool _isInterstitialLoaded;
//
//    #region Standart staff
//    
//    private void Awake()
//    {
//        #if UNITY_ANDROID
//           Appodeal.requestAndroidMPermissions(this);
//	    #endif
//    }
//    
//    private void Start()
//    {   
//		Appodeal.setLogLevel(loggingMode ? Appodeal.LogLevel.Verbose : Appodeal.LogLevel.None);
//        Appodeal.setTesting(testingMode);
//        
//        Appodeal.disableLocationPermissionCheck();
//        Appodeal.disableWriteExternalStoragePermissionCheck();
//        
//        Appodeal.initialize(AppKey, Appodeal.INTERSTITIAL | Appodeal.REWARDED_VIDEO | Appodeal.BANNER, false);
//        
//        Appodeal.setInterstitialCallbacks (this);
//        Appodeal.setRewardedVideoCallbacks(this);
//        //Appodeal.setBannerCallbacks(this);
//        
//        StartCoroutine(DelayedAction(InterstitialReady, 240f));
//    }
//    
//    private void OnApplicationFocus(bool hasFocus) 
//    {
//        Debug.Log("Focus = " + hasFocus);
//        
//        if (hasFocus) 
//        {
//            Appodeal.onResume();
//        }
//    }
//    
//    private void OnEnable() {
//        GlobalEvents<OnAdsVideoShow>.Happened += ShowVideo;
//        GlobalEvents<OnAdsRewardedShow>.Happened += ShowRewarded;
//        GlobalEvents<OnAdsBannerShow>.Happened += OnAdsBannerShow;
//        GlobalEvents<OnAdsBannerHide>.Happened += OnAdsBannerHide;
//    }
//    #endregion
//
//    private static IEnumerator DelayedAction(Action action, float seconds)
//    {
//         yield return new WaitForSeconds(seconds);
//         action();
//    }
//
//    private void ShowVideo(OnAdsVideoShow obj)
//    {
//        if (!_isInterstitialLoaded || !_isInterstitialReady) return;
//        _isInterstitialReady = false;
//        
//        if (!ShowInterstitial())
//        {
//            onInterstitialClosed();
//        }
//    }
//
//    private void InterstitialReady()
//    {
//        _isInterstitialReady = true;
//        Debug.Log("InterstitialReady");
//    }
//
//    private void ShowRewarded(OnAdsRewardedShow obj)
//    {
//        _isInterstitialReady = false;
//        
//        if (_isRewardedLoaded)
//        {
//            if (!ShowRewardedVideo())
//            {
//                onRewardedVideoClosed(false);
//            }
//        }
//        else
//        {
//            onRewardedVideoClosed(true);
//        }
//    }
//    
//    private void OnAdsBannerShow(OnAdsBannerShow obj)
//    {
//        StartCoroutine (ShowBanner());
//    }
//    
//    private void OnAdsBannerHide(OnAdsBannerHide obj)
//    {
//        HideBottomBanner();
//    }
//
//    private static bool ShowInterstitial()
//    {
//        FB.LogAppEvent ("ad_shown_interstitial");
//        return Appodeal.show(Appodeal.INTERSTITIAL);
//    }
//    
//    #region Interstitial callback handlers
//
//    public void onInterstitialLoaded(bool isPrecache)
//    {
//        print("Interstitial loaded");
//        _isInterstitialLoaded = true;
//    }
//    public void onInterstitialFailedToLoad() { print("Interstitial failed to load"); }
//    public void onInterstitialShown() { print("Interstitial shown"); }
//    public void onInterstitialClicked() { print("Interstitial clicked"); }
//
//    public void onInterstitialClosed()
//    {
//        StartCoroutine(DelayedAction(InterstitialReady, 90f));
//        print("Interstitial closed");
//        GlobalEvents<OnAdsVideoClosed>.Call(new OnAdsVideoClosed());
//    }
//    public void onInterstitialExpired() { print("Appodeal. Interstitial expired"); }
//
//    #endregion
//
//    private static bool ShowRewardedVideo()
//    {
//        if (!Appodeal.canShow(Appodeal.REWARDED_VIDEO)) return false;
//        return Appodeal.show(Appodeal.REWARDED_VIDEO);
//    }
//    
//    #region Rewarded Video callback handlers
//
//    public void onRewardedVideoLoaded(bool isPrecache) {
//        _isRewardedLoaded = true;
//        print("Appodeal. Video loaded"); 
//    }
//    public void onRewardedVideoFailedToLoad() { print("Rewarded video failed to load"); }
//    public void onRewardedVideoShown() { print("Rewarded video shown"); }
//    public void onRewardedVideoFinished(double amount, string name) { print("Appodeal. Reward: " + amount + " " + name); }
//    public void onRewardedVideoClosed(bool finished)
//    {
//        StartCoroutine(DelayedAction(InterstitialReady, 90f));
//        print("Rewarded video closed and finished = " + finished );
//        GlobalEvents<OnAdsRewardedClosed>.Call(new OnAdsRewardedClosed{IsReward = finished});
//    }
//    public void onRewardedVideoExpired() { print("Appodeal. Video expired"); }
//
//    #endregion
//
//    private IEnumerator ShowBanner() {
//        while (true) {
////            if (Appodeal.isLoaded(Appodeal.BANNER_BOTTOM))
////            {
//                if (Appodeal.show(Appodeal.BANNER_BOTTOM))
//                {
//                    FB.LogAppEvent ("banner_shown");
//                    yield break;
//                }
////            }
//
//            yield return new WaitForSeconds (3f);
//        }
//    }
//    
//    private void HideBottomBanner()
//    {
//        Appodeal.hide(Appodeal.BANNER_BOTTOM);
//    }
//    
//    #region Banner callback handlers
//    
//    public void onBannerLoaded(bool precache) { print("banner loaded"); }
//    public void onBannerFailedToLoad() { print("banner failed"); }
//    public void onBannerShown() { print("banner opened"); }
//    public void onBannerClicked() { print("banner clicked"); }
//    public void onBannerExpired() { print("banner expired");  }
//    
//    #endregion
//
//    #region Permission Grant callback handlers
//    
//    public void writeExternalStorageResponse(int result) 
//    { 
//        if (result == 0) 
//        {
//            Debug.Log("WRITE_EXTERNAL_STORAGE permission granted"); 
//        }
//        else
//        {
//            Debug.Log("WRITE_EXTERNAL_STORAGE permission grant refused"); 
//        }
//    }
//    
//    public void accessCoarseLocationResponse(int result) 
//    { 
//        if(result == 0) 
//        {
//            Debug.Log("ACCESS_COARSE_LOCATION permission granted"); 
//        }
//        else
//        {
//            Debug.Log("ACCESS_COARSE_LOCATION permission grant refused"); 
//        }
//    }
//    
//    #endregion
//}
