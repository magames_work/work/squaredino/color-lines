﻿using UnityEngine;
//using FlurrySDK;

namespace SquareDino.CurrentAPIs
{
    public class MyFlurry : MonoBehaviour
    {
        [SerializeField] private string FLURRY_ANDROID_API_KEY;
        [SerializeField] private string FLURRY_IOS_API_KEY;

        private void Start()
        {

            string apiKey;
#if UNITY_ANDROID
        apiKey = FLURRY_ANDROID_API_KEY;
#elif UNITY_IOS
            apiKey = FLURRY_IOS_API_KEY;
#endif
//            // Initialize Flurry once.
//            new Flurry.Builder()
//                .WithCrashReporting(true)
//                .WithLogEnabled(true)
//                .WithLogLevel(Flurry.LogLevel.LogVERBOSE)
//                .Build(apiKey);
//
//            // Example to get Flurry versions.
//            Debug.Log("AgentVersion: " + Flurry.GetAgentVersion());
//            Debug.Log("ReleaseVersion: " + Flurry.GetReleaseVersion());
//
//            // Log Flurry events.
//            var status = Flurry.LogEvent("Unity Event");
//            Debug.Log("Log Unity Event status: " + status);
//
////        // Log Flurry timed events with parameters.
////        IDictionary<string, string> parameters = new Dictionary<string, string>();
////        parameters.Add("Author", "Flurry");
////        parameters.Add("Status", "Registered");
////        status = Flurry.LogEvent("Unity Event Params Timed", parameters, true);
////        Debug.Log("Log Unity Event with parameters timed status: " + status);
//
////        Flurry.EndTimedEvent("Unity Event Params Timed");
        }
    }
}