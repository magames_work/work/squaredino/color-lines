﻿using UnityEngine;

namespace SquareDino.CurrentAPIs
{
    public class MyAppsFlyer : MonoBehaviour {
    
        [SerializeField] private string DevKey = "zTMKaYxjJTqRGByUFka8Td";
        [SerializeField] private string AppId_iOS = "Number Only";
        [SerializeField] private string GooglePlayPackageName = "YOUR_ANDROID_PACKAGE_NAME_HERE";

        
        private void Start()
        {
    #if FLAG_AF
                AppsFlyer.setAppsFlyerKey(DevKey);
                /* AppsFlyer.setIsDebug (true); */
        #if UNITY_IOS
            AppsFlyer.setAppID(AppId_iOS);
            AppsFlyer.trackAppLaunch();
        #elif UNITY_ANDROID
           AppsFlyer.setAppID (GooglePlayPackageName);
           /* For getting the conversion data in Android, you need to add the "AppsFlyerTrackerCallbacks" listener.*/
           AppsFlyer.init (DevKey,"AppsFlyerTrackerCallbacks");
        #endif
    #endif    
        }

    }
}