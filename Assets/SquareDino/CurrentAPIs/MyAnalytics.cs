﻿using UnityEngine;

#if !UNITY_EDITOR
        using System.Collections.Generic;
        using UnityEngine.Analytics;
#endif

#if FLAG_FB
        using Facebook.Unity;
#endif

#if FLAG_GA
        using GameAnalyticsSDK;
#endif

namespace Assets.Scripts.SquareDino.CurrentAPIs
{
    public class MyAnalytics : MonoBehaviour
    {
        public static void LevelStart(int currentLevel)
        {
                #if !UNITY_EDITOR
                        Analytics.CustomEvent("Level Start", new Dictionary<string, object> {{"Id", currentLevel}});
                #endif
                                
                #if FLAG_FB
                        FB.LogAppEvent ("game_start");
                #endif
                                
                #if FLAG_GA
                        GameAnalytics.NewProgressionEvent(GAProgressionStatus.Start, "Level " + currentLevel);
                #endif
        }

        public static void LevelFailed(int currentLevel, int score = 0)
        {
                #if !UNITY_EDITOR
                        Analytics.CustomEvent("Level Failed", new Dictionary<string, object> {{"Id", currentLevel}});
                #endif
                                
                #if FLAG_FB
                        FB.LogAppEvent ("game_end");
                        //FB.LogAppEvent ("game_end_levels");
                #endif
                                
                #if FLAG_GA
                        GameAnalytics.NewProgressionEvent(GAProgressionStatus.Fail, "Level" + currentLevel, score);
                #endif
        }

        public static void LevelWin(int currentLevel, int score = 0)
        {
                #if !UNITY_EDITOR
                        Analytics.CustomEvent("Level Finished", new Dictionary<string, object> {{"Id", currentLevel}});
                #endif
                
                #if FLAG_FB
                        FB.LogAppEvent ("game_end");
                #endif
                                
                #if FLAG_GA
                        GameAnalytics.NewProgressionEvent(GAProgressionStatus.Complete, "Level" + currentLevel, score);
                #endif
        }
    
        public static void OnAdsShowInterstitial()
        {
                #if FLAG_FB
                        FB.LogAppEvent ("ad_shown_interstitial");
                #endif
                                
                #if FLAG_GA

                #endif
        }
    
        public static void OnAdsShowBanner()
        {
                #if FLAG_FB
                        FB.LogAppEvent ("banner_shown");
                #endif
                                
                #if FLAG_GA

                #endif
        }
    
        public static void OnAdsShowRewarded(bool finished)
        {
                #if FLAG_FB
                        FB.LogAppEvent ("rewarded_shown: " + finished);
                #endif
        }

        public static void NewHighScore(int value)
        {
//#if !UNITY_EDITOR
//        Analytics.CustomEvent("HighScore", new Dictionary<string, object> {{"sessions", PrefsManager.GameplayCounter}, {"score", value}});
//#endif
        }

        public static void RateClick()
        {
//#if !UNITY_EDITOR
//        Analytics.CustomEvent("RateFeedbackClick",
//            new Dictionary<string, object> {{"sessions", PrefsManager.GameplayCounter}});
//#endif
        }
    }
}