﻿using UnityEngine;
#if FLAG_GA
	using GameAnalyticsSDK;
#endif

public class MyGameAnalytics : MonoBehaviour
{
	private void Start()
	{
		#if FLAG_GA
			GameAnalytics.Initialize();
		#endif
	}
}