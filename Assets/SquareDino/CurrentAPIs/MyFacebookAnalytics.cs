﻿using UnityEngine;
#if FLAG_FB
    using Facebook.Unity;
#endif

namespace SquareDino.CurrentAPIs
{
    public class MyFacebookAnalytics : MonoBehaviour
    {
        #if FLAG_FB
        private void Awake()
        {
            if (!FB.IsInitialized)
            {
                FB.Init(InitCallback);
            }
            else
            {
                FB.ActivateApp();
            }
        }

        private void InitCallback()
        {
            if (FB.IsInitialized)
            {
                // Signal an app activation App Event
                FB.ActivateApp();
                Debug.Log("Facebook SDK - Initialized");
                // Continue with Facebook SDK
                // ...
            }
            else
            {
                Debug.Log("Failed to Initialize the Facebook SDK");
            }
        }
        #endif
    }
}