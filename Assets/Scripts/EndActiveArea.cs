﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class EndActiveArea : MonoBehaviour
{
    public enum Colors {Red, Blue, Yellow, Green, Orange, Violet}
    public Colors color;
    public int endId;
    public bool complited =false;
    private TexturePainter texturePainter;
    void Start()
    {
        texturePainter = FindObjectOfType<TexturePainter>();

        gameObject.GetComponent<SpriteRenderer>().sprite = texturePainter.EndActiveArea;
        gameObject.transform.localScale = Vector3.one * texturePainter.AreaSize;
        gameObject.GetComponentInChildren<SphereCollider>().radius = texturePainter.EndAreaColliderSize;
        if(color == Colors.Red)
        {  
            GetComponent<SpriteRenderer>().color = texturePainter.Red;
            endId = 1;
        }
        if(color == Colors.Blue)
        {   
            GetComponent<SpriteRenderer>().color = texturePainter.Blue;
            endId = 2;
        }
        if(color == Colors.Yellow)
        {
            GetComponent<SpriteRenderer>().color = texturePainter.Yellow;
            endId = 3;
        }
        if(color == Colors.Green)
        {  
            GetComponent<SpriteRenderer>().color = texturePainter.Green;
            endId = 4;
        }
        if(color == Colors.Orange)
        {   
            GetComponent<SpriteRenderer>().color = texturePainter.Orange;
            endId = 5;
        }
        if(color == Colors.Violet)
        {
            GetComponent<SpriteRenderer>().color = texturePainter.Violet;
            endId = 6;
        }
    }

    private void Update() {
        if(gameObject.GetComponent<EndActiveArea>().complited)
        {
            gameObject.GetComponent<SpriteRenderer>().sprite = texturePainter.StartActiveArea;
        }
    }
}
