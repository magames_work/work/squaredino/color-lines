﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using TMPro;

public class UIManager : MonoBehaviour
{
    public LVLManager lvlManager;
    [Header("TextMeshPro")]
    public TextMeshProUGUI NextLVL;
    public TextMeshProUGUI LvlCounter, lvlcleared;
    public Text fps;

    [Header("GameObjects")]
    public GameObject instr;
    public GameObject LvlClearedPanel, shopfreespace, nextlvlbutton;

    [Header("Rect Transform")]
    public RectTransform ShopButton;
    public RectTransform VibroButton, ShopPanel, Tip;

    [Header("ProgressBar")]
    public Slider progressBar;

    [Header("Particle System")]
    public ParticleSystem FireworkParticleSystem;

    private bool IsMenuSwiped;

    int current;
    private void Update() 
    {
        current = (int)(1f / Time.unscaledDeltaTime);
        fps.text = current.ToString();
        if(IsMenuSwiped)
        {
            if(Input.GetMouseButtonDown(0))
            {
                Tip.GetComponent<Image>().DOFade(0, 1);
                //Invoke("tipdisable", 1f);
            }
        }
    }

    public void LvlCountUpdate()
    {
        LvlCounter.text = "" + lvlManager.currentlvlUI;
        NextLVL.text = "" + lvlManager.nextlvlUI;
    }
    public void LvlCountUpdateCustom()
    {
        LvlCounter.text = "" + lvlManager.currentlvl;
        NextLVL.text = "" + lvlManager.nextLVL;
    }

    public void ResetProgressBar()
    {
        DOTween.To(() => progressBar.value, x => progressBar.value = x, 0, .5f);
    }

    public void OnPaint()
    {
        progressBar.maxValue = lvlManager.Levels[lvlManager.currentlvl].paintNeed;
        DOTween.To(() => progressBar.value, x => progressBar.value = x, lvlManager.paintCount, .5f);
    }
    public void SetCustomLvL()
    {
        LvlCountUpdate();
        ResetProgressBar();
        lvlManager.StartSlide();
    }

    public void LvlCleared()
    {
        StartCoroutine(Lvlcleared());
    }

    IEnumerator Lvlcleared()
    {
        FireworkParticleSystem.Play();
        DragButtonsOn();
        //gameplay.enabled = false;

        LvlClearedPanel.GetComponent<Button>().enabled = false;
        lvlcleared.text = "Level Cleared!";

        yield return new WaitForSeconds(1.4f);
        LvlClearedPanel.SetActive(true);
        LvlClearedPanel.GetComponent<Button>().enabled = true;
        nextlvlbutton.SetActive(true);
    }

    public void GameComplited()
    {
        StartCoroutine(gamecomplited());
    }

    IEnumerator gamecomplited()
    {
        FireworkParticleSystem.Play();
        DragButtonsOn();
        //gameplay.enabled = false;

        LvlClearedPanel.GetComponent<Button>().enabled = false;
        lvlcleared.text = "Level Cleared!";

        yield return new WaitForSeconds(1.4f);
        LvlClearedPanel.SetActive(true);
        LvlClearedPanel.GetComponent<Button>().enabled = true;
        nextlvlbutton.SetActive(true);
    }

    public void DragButtonsOut()
    {
        lvlcleared.text = "";
        ShopButton.DOAnchorPosX(-200, 1);
        VibroButton.DOAnchorPosX(200, 1);
    }

    public void DragButtonsOn()
    {
        ShopButton.DOAnchorPosX(100, .5f);
        VibroButton.DOAnchorPosX(-100, .5f);
    }

    public void DragPanelOn()
    {
        ShopPanel.DOAnchorPos(Vector2.zero, 0.5f);
        shopfreespace.SetActive(true);
    }

    public void DragPanelOut()
    {
        StartCoroutine(SwipePanelOut());
    }

    IEnumerator SwipePanelOut()
    {
        ShopPanel.DOAnchorPos(new Vector2(0, -1000), 0.5f);
        yield return new WaitForSeconds(0.5f);
        shopfreespace.SetActive(false);
    }

    public void LvlEnter()
    {
        if (!IsMenuSwiped)
        {
            StartCoroutine(SwipePanelOut());
            DragButtonsOut();
            Tip.GetComponent<Image>().DOFade(0, 1);
            //Invoke("tipdisable", 1f);
            IsMenuSwiped = true;
        }
    }
    void tipdisable()
    {
        Tip.gameObject.SetActive(false);
    }

    public void LvlEscape()
    {
        IsMenuSwiped = false;
    }
    public void tipLogic()
    {
        if(lvlManager.currentlvl == 0)
        {
            instr.SetActive(true);
            Tip.GetComponent<Image>().DOFade(0, 0);
            Tip.GetComponent<Image>().DOFade(0.7f, 1f);
            Tip.gameObject.SetActive(true);
            Tip.GetComponent<Animator>().SetInteger("number", 1);
        }
        if(lvlManager.currentlvl == 8)
        {
             
            instr.SetActive(true);
            Tip.GetComponent<Image>().DOFade(0, 0);
            Tip.GetComponent<Image>().DOFade(0.7f, 1f);
            Tip.gameObject.SetActive(true);
            Tip.GetComponent<Animator>().SetInteger("number", 2);
        }
        // if(lvlManager.currentlvl == 12)
        // {
            
        // }
        if(lvlManager.currentlvl != 0 && lvlManager.currentlvl != 8 && lvlManager.currentlvl != 18)
        {
            instr.gameObject.SetActive(false);
        }
    }
    public void show18()
    {
        instr.SetActive(true);
        Tip.GetComponent<Image>().DOFade(0, 0);
        Tip.GetComponent<Image>().DOFade(0.7f, 1f);
        Tip.gameObject.SetActive(false);
        Tip.gameObject.SetActive(true);
        Tip.gameObject.GetComponent<Animator>().SetInteger("number", 3);
    }
}
