﻿using UnityEngine;
using System.Collections.Generic;
using System.Collections;
using UnityEditor;

public class TexturePainter : MonoBehaviour {

#if UNITY_EDITOR
    [CustomEditor(typeof(TexturePainter))]
    public class texturePainterGUI : Editor
    {
        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();
			
            TexturePainter texturePainter = FindObjectOfType<TexturePainter>();

            texturePainter.FreeDraw = GUILayout.Toggle(texturePainter.FreeDraw, "Free Draw");

            if (texturePainter.FreeDraw)
                texturePainter.color = (_Colors)EditorGUILayout.EnumPopup(texturePainter.color);
        }
    }
#endif

#region variables

[Header("Scripts")]
	public LVLManager lvlmanager;
	public UIManager uimanager;
	public VibroManager vibro;


[Header("Containers")]
	public GameObject brushContainer;
	public GameObject SensorContainer;
	public GameObject ConnectedLinesContainer;
	public GameObject ObstaclesContainer;
	public GameObject SplatContainer;
	

[Header("Components")]
	public Camera canvasCam;
	public RenderTexture canvasTexture;
	public Material baseMaterial;
	public Material defaultSpriteMat;
	public GameObject BrushPref;
	public GameObject SensorPref;
	public GameObject page;
	public Sprite EndActiveArea;
	public Sprite StartActiveArea;
	public Color Red;
	public Color Blue;
	public Color Yellow;
	public Color Green;
	public Color Orange;
	public Color Violet;
	public Color Gray;
	

[Header("Configuration")]
	public float brushSize=0.07f;
	public float splatersSize;
	public float colliderSize = 0.03f;
	public float AreaSize;
	public float EndAreaColliderSize = 1;
	public float sensorInterval = 0.03f;
	public float vibroInterval = 1;
	public float mixInterval;
	private Vector2 mousePos;
	public float factor;
	
	private Vector2 catchMousePos;
	bool latemix = false;
	[HideInInspector]
	public Color brushColor;
	[HideInInspector]
	public enum _Colors {Red, Blue, Yellow, Green, Orange, Violet}
	[HideInInspector]
	public _Colors color;
	[HideInInspector]
	public bool FreeDraw;
	[HideInInspector]
	public bool saving=false;
	[HideInInspector]
	public List<GameObject> line = new List<GameObject>();
	[HideInInspector]
	public bool clicked;
	[HideInInspector]
	public bool isStart = false;
	[HideInInspector]
	public bool obstLineCreated = false;
	[HideInInspector]
	public Vector3 catchmousepos;
	private Vector2 cmp;
	[HideInInspector]
	public GameObject startmix;
	private LineInstance instance;
#endregion

	void Start()
	{
		cmp = Input.mousePosition;
	}
	void Update ()
	{
		if(Input.GetMouseButton(0)) mousePos = new Vector2(Input.mousePosition.x, Input.mousePosition.y);
		if(Input.GetMouseButtonDown(0)) catchMousePos = mousePos;
		instance = FindObjectOfType<LineInstance>();

		if (clicked == true)
		{
			DoAction();
		}
		if(!clicked)
		{
			try{
				instance.currentid = 0;
				instance.MixEnable = false;
				latemix = false;
			}catch{}
		}
	}

	
    public void DoAction()
    {
		GameObject splatter;
		if(instance.currentid == 1)
		{
			brushColor = Red;
		}
		if(instance.currentid == 2)
		{
			brushColor = Blue;
		}
		if(instance.currentid == 3)
		{
			brushColor = Yellow;
		}
		if(instance.currentid == 4)
		{
			brushColor = Green;
		}
		if(instance.currentid == 5)
		{
			brushColor = Orange;
		}
		if(instance.currentid == 6)
		{
			brushColor = Violet;
		}
		if(instance.currentid == 10)
		{
			brushColor = Gray;
		}
		if (saving)
			return;
		Vector3 uvWorldPosition=Vector3.zero;
		if(HitTestUVPosition(ref uvWorldPosition))
		{
			
			
			if(mousePos.x > catchMousePos.x + sensorInterval | mousePos.y > catchMousePos.y + sensorInterval) spawn();
			if(mousePos.x < catchMousePos.x - sensorInterval | mousePos.y < catchMousePos.y - sensorInterval) spawn();
			var dir = mousePos - catchMousePos;
		}

		Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
		RaycastHit hit; 
		if(Physics.Raycast(ray, out hit))
		{
			if(hit.transform.tag == "endArea" | hit.transform.tag == "startArea" | hit.transform.tag == "canvas")
			{
				splatter = (GameObject)Instantiate(Resources.Load("Instances/Splatters"), SplatContainer.transform);
				splatter.transform.localScale = Vector3.one * splatersSize;
				splatter.transform.position = hit.point;
			}
		}

		if(instance.MixEnable)
		{
			Ray ray1 = Camera.main.ScreenPointToRay(Input.mousePosition);
			RaycastHit hit1;
			if(Physics.Raycast(ray1, out hit1)){}
			Vector3 mousepos = Input.mousePosition;
			
			if(mousepos.x > catchmousepos.x + mixInterval | mousepos.y > catchmousepos.y + mixInterval) 
			{
				latemix = true;
			}
			if(mousepos.x < catchmousepos.x - mixInterval | mousepos.y < catchmousepos.y - mixInterval) 
			{
				latemix = true;
			}
		}

		if(latemix)
		{
			try
			{
				sensor.GetComponent<Sensor>().isMixed = true;
				startmix.GetComponentInParent<StartActiveArea>().isMixed = true;
			}
			catch{}
		}
	}

	GameObject brushObj;
	GameObject sensor;
	GameObject ObstLine;

	void spawn()
	{
		var dir = mousePos - catchMousePos;
		Vector3 uvWorldPosition=Vector3.zero;
		if(HitTestUVPosition(ref uvWorldPosition))
		{
			if(FreeDraw)
			{
				if(!obstLineCreated)
				{
					ObstLine = Instantiate(new GameObject("ObstacleLine"), ObstaclesContainer.transform);
					obstLineCreated = true;
					ObstLine.tag = "Obstacle";
				}
				brushObj=(GameObject)Instantiate(BrushPref, ObstLine.transform);
				sensor = (GameObject)Instantiate(SensorPref, ObstLine.transform);
			}
			else
			{
				brushObj=(GameObject)Instantiate(BrushPref, brushContainer.transform);
				sensor = (GameObject)Instantiate(SensorPref, SensorContainer.transform);
			}
			brushObj.GetComponent<SpriteRenderer>().color=brushColor;
			brushObj.GetComponent<brushColor>().colorid = instance.currentid;
			brushObj.transform.localPosition = uvWorldPosition;
			var angle = Mathf.Atan2(dir.y, dir.x) * Mathf.Rad2Deg;
			brushObj.transform.localRotation = Quaternion.AngleAxis(angle - 6, Vector3.forward);
			
			
			if(dir.magnitude * factor < brushSize) brushObj.transform.localScale = new Vector3(brushSize, brushSize, brushSize);
			else brushObj.transform.localScale = new Vector3(dir.magnitude * factor, brushSize, brushSize);
			if(dir.magnitude * colliderSize < brushSize) sensor.transform.localScale = new Vector3(0.03f, brushSize, brushSize);
			else sensor.transform.localScale = new Vector3(dir.magnitude * colliderSize, brushSize, brushSize);

			line.Add(brushObj);
			
			if(mousePos.x > cmp.x + vibroInterval | mousePos.y > cmp.y + vibroInterval) 
			{
				vibro.TriggerLightImpact();
				cmp = Input.mousePosition;
			}
			if(mousePos.x < cmp.x - vibroInterval | mousePos.y < cmp.y - vibroInterval) 
			{
				vibro.TriggerLightImpact();
				cmp = Input.mousePosition;
			}
			
			sensor.transform.localPosition = uvWorldPosition;
			sensor.transform.rotation = Quaternion.AngleAxis(angle, Vector3.forward);
			sensor.GetComponent<Sensor>().sensorid = instance.currentid;
			
			line.Add(sensor);
			catchMousePos = mousePos;
		}
	}

	bool HitTestUVPosition(ref Vector3 uvWorldPosition)
    {
		RaycastHit hit;
		Vector3 cursorPos = new Vector3 (Input.mousePosition.x, Input.mousePosition.y, 0.0f);
		Ray cursorRay=Camera.main.ScreenPointToRay (cursorPos);
		if (Physics.Raycast(cursorRay,out hit,200, 1)){
			MeshCollider meshCollider = hit.collider as MeshCollider;
			if (meshCollider == null || meshCollider.sharedMesh == null)
				return false;
			Vector2 pixelUV  = new Vector2(hit.textureCoord.x,hit.textureCoord.y);
			uvWorldPosition.x=pixelUV.x-canvasCam.orthographicSize;//To center the UV on X
			uvWorldPosition.y=pixelUV.y-canvasCam.orthographicSize;//To center the UV on Y
			uvWorldPosition.z=0.0f;
			return true;
		}
		else{
			return false;
		}

	}

	public void SaveTexture()
	{
		System.DateTime date = System.DateTime.Now;
		RenderTexture.active = canvasTexture;
		Texture2D tex = new Texture2D(canvasTexture.width, canvasTexture.height, TextureFormat.RGB24, false);
		tex.ReadPixels (new Rect (0, 0, canvasTexture.width, canvasTexture.height), 0, 0);
		tex.Apply ();
		RenderTexture.active = null;
		baseMaterial.mainTexture =tex;
		foreach (Transform child in brushContainer.transform) {
			Destroy(child.gameObject);
		}
	}

	public void ResetPaint()
	{
		Texture2D tex = new Texture2D(canvasTexture.width, canvasTexture.height, TextureFormat.RGB24, false);

		// Reset all pixels color to transparent
		Color32 resetColor = new Color32(255, 255, 255, 255);
		Color32[] resetColorArray = tex.GetPixels32();

		for (int i = 0; i < resetColorArray.Length; i++) {
			resetColorArray[i] = resetColor;
		}

		tex.SetPixels32(resetColorArray);
		tex.Apply();
		baseMaterial.mainTexture =tex;
	}


	#if !UNITY_WEBPLAYER
		IEnumerator SaveTextureToFile(Texture2D savedTexture){

			string fullPath=System.IO.Directory.GetCurrentDirectory()+"\\UserCanvas\\";
			System.DateTime date = System.DateTime.Now;
			string fileName = "CanvasTexture.png";
			if (!System.IO.Directory.Exists(fullPath))
				System.IO.Directory.CreateDirectory(fullPath);
			var bytes = savedTexture.EncodeToPNG();
			System.IO.File.WriteAllBytes(fullPath+fileName, bytes);
			Debug.Log ("<color=orange>Saved Successfully!</color>"+fullPath+fileName);
			yield return null;
		}
	#endif
}
