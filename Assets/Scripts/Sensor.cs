﻿using UnityEngine;

public class Sensor : MonoBehaviour
{
    TexturePainter texturePainter;
    LineInstance instance;
    public int sensorid;
    public bool isMixed;
    void Start()
    {
        instance = FindObjectOfType<LineInstance>();
        texturePainter = FindObjectOfType<TexturePainter>();
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        
    //Violet (Red + Blue)
            if(collision.GetComponent<Sensor>().sensorid == 1 & sensorid == 2 || collision.GetComponent<Sensor>().sensorid == 2 & sensorid == 1)
            {
                instance.currentid = 6;
                sensorid = 6;
                texturePainter.catchmousepos = Input.mousePosition;
                instance.MixEnable = true;
            }
    //Orange (Red + Yellow)
            if(collision.GetComponent<Sensor>().sensorid == 1 & sensorid == 3 || collision.GetComponent<Sensor>().sensorid == 3 & sensorid == 1)
            {
                instance.currentid = 5;
                sensorid = 5;
                texturePainter.catchmousepos = Input.mousePosition;
                instance.MixEnable = true;
            }

    //Green (Blue + Yellow)
            if(collision.GetComponent<Sensor>().sensorid == 2 & sensorid == 3 || collision.GetComponent<Sensor>().sensorid == 3 & sensorid == 2)
            {
                instance.currentid = 4;
                sensorid = 4;
                texturePainter.catchmousepos = Input.mousePosition;
                instance.MixEnable = true;
            }
    //gray
            try
            {
                if(collision.GetComponent<Sensor>().sensorid == 4 & instance.currentid == 1 || collision.GetComponent<Sensor>().sensorid == 4 & instance.currentid == 2 || collision.GetComponent<Sensor>().sensorid == 4 & instance.currentid == 3)
                {
                    Gray();
                }
                if(collision.GetComponent<Sensor>().sensorid == 5 & instance.currentid == 1 || collision.GetComponent<Sensor>().sensorid == 5 & instance.currentid == 2 || collision.GetComponent<Sensor>().sensorid == 5 & instance.currentid == 3)
                {
                    Gray();
                }
                if(collision.GetComponent<Sensor>().sensorid == 6 & instance.currentid == 1 || collision.GetComponent<Sensor>().sensorid == 6 & instance.currentid == 2 || collision.GetComponent<Sensor>().sensorid == 6 & instance.currentid == 3)
                {
                    Gray();
                }

        
                if(instance.currentid == 4 || instance.currentid == 5 || instance.currentid == 6)
                {
                    if(collision.GetComponent<Sensor>().isMixed == true & instance.currentid != sensorid)
                    {
                        Gray();
                    }
                }
            }catch{}
        
    }
    
    void Gray()
    {
        instance.isRestart = true;
        instance.currentid = 10;
        Invoke("Clean", 0.3f);
        texturePainter.vibro.TriggerFailure();
        texturePainter.lvlmanager.Invoke("RestartLvl", 1f);
    }

    void Clean()
    {
        texturePainter.clicked = false;
    }
}