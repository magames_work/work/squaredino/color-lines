﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SplatColor : MonoBehaviour
{
    TexturePainter texturePainter;
    void Start()
    {
        texturePainter = FindObjectOfType<TexturePainter>();
        if (gameObject.GetComponent<ParticleSystem>())
        {
            GetComponent<Renderer>().material.color = texturePainter.brushColor;
        }
    }
}
