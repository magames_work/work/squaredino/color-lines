﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class brushColor : MonoBehaviour
{
    public int colorid;
    private TexturePainter texturePainter;

    private void Start() {
        texturePainter = FindObjectOfType<TexturePainter>();
        if(colorid == 1)
        {  
            GetComponent<SpriteRenderer>().color = texturePainter.Red;
        }
        if(colorid == 2)
        {   
            GetComponent<SpriteRenderer>().color = texturePainter.Blue;
        }
        if(colorid == 3)
        {
            GetComponent<SpriteRenderer>().color = texturePainter.Yellow;
        }
        if(colorid == 4)
        {  
            GetComponent<SpriteRenderer>().color = texturePainter.Green;
        }
        if(colorid == 5)
        {   
            GetComponent<SpriteRenderer>().color = texturePainter.Orange;
        }
        if(colorid == 6)
        {
            GetComponent<SpriteRenderer>().color = texturePainter.Violet;
        }
    }
}

