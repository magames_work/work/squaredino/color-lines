﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LVLManager : MonoBehaviour
{
//Lists
    public List<Lvls> Levels = new List<Lvls>();
    [Serializable]
    public class Lvls
    {
        public GameObject paintObj;
        public int paintNeed;
    }

//Classes
    private TexturePainter texturePainter;
    
    [HideInInspector]
    public int nextLVL;
    [HideInInspector]
    public int currentlvl;
    [HideInInspector]
    public int currentlvlUI;
    [HideInInspector]
    public int nextlvlUI;
    [HideInInspector]
    public bool Debug;
    public int paintCount;
    public int LvlAfterEnd;
    public bool SaveTexture;
    private int CountNextLVL;
    private GameObject currentObj, NextObj;
    

    private void Awake() {
        Application.targetFrameRate = 60;
    }
    private void Start()
    {
        texturePainter = FindObjectOfType<TexturePainter>();

        if (!PlayerPrefs.HasKey("Saved LVL")) currentlvl = 0;
        else currentlvl = PlayerPrefs.GetInt("Saved LVL");
        if (!PlayerPrefs.HasKey("Saved LVL UI")) currentlvlUI = currentlvl;
        else currentlvlUI = PlayerPrefs.GetInt("Saved LVL UI");

        CalculateNextLvl();
        texturePainter.uimanager.LvlCounter.text = "" + currentlvlUI;
        texturePainter.uimanager.NextLVL.text = "" + nextlvlUI;
        
        RestartLvl();
        CalculatePaintNeed();
        texturePainter.uimanager.tipLogic();
    }

    private void Update()
    {
        CalculateNextLvl();
        if (paintCount >= Levels[currentlvl].paintNeed)
        {
            NextLvl();
            var sprites = FindObjectsOfType<SpriteRenderer>();
            texturePainter.uimanager.LvlCleared();
        }
        
    }

    public void NextLvl()
    {
        if (currentlvl < Levels.Count - 1)
        {
            currentlvl++;
            currentlvlUI++;
            PlayerPrefs.SetInt("Saved LVL", currentlvl);
            PlayerPrefs.SetInt("Saved LVL UI", currentlvlUI);
            texturePainter.vibro.TriggerSuccess();
            texturePainter.clicked = false;
            texturePainter.enabled = false;
            paintCount = -1;
        }
        else if (currentlvl >= Levels.Count - 1)
        {
            currentlvl = LvlAfterEnd;
            currentlvlUI++;
            PlayerPrefs.SetInt("Saved LVL", currentlvl);
            PlayerPrefs.SetInt("Saved LVL UI", currentlvlUI);
            texturePainter.uimanager.GameComplited();
            texturePainter.vibro.TriggerSuccess();
            texturePainter.clicked = false;
            texturePainter.enabled = false;
            paintCount = -1;
        }
    }

    public void ResetGame()
    {
        PlayerPrefs.DeleteAll();
    }

    public void StartSlide()
    {
        StartCoroutine(Slide());
    }

    public IEnumerator Slide()
    {
        texturePainter.uimanager.tipLogic();
        Destroy(currentObj);
        Clean();
        texturePainter.ResetPaint();
        NextObj = Instantiate(Levels[currentlvl].paintObj, GameObject.Find("_MainScene").transform) as GameObject;
        
        changeObstalceColor();
        yield return new WaitForSeconds(0.5f);
        texturePainter.enabled = true;

        if(SaveTexture)
        {
            texturePainter.SaveTexture();
            ClearObst();
        }
        //GameObject preObj = currentObj;
        currentObj = NextObj;
        ResetPaintCount();
        CalculatePaintNeed();

        //Destroy(preObj.gameObject);
        texturePainter.clicked =false;
    }

    public void RestartLvl()
    {
        StartCoroutine(restartlvl());
    }
    IEnumerator restartlvl()
    {
        Destroy(currentObj);
        texturePainter.clicked = false;
        texturePainter.enabled = false;
        Clean();
        texturePainter.ResetPaint();
        currentObj = Instantiate((GameObject)Levels[currentlvl].paintObj, GameObject.Find("_MainScene").transform);
        iTween.FadeFrom(texturePainter.page, 0, 0f);
        iTween.FadeTo(texturePainter.page, 1f, 0.6f);
        changeObstalceColor();
        yield return new WaitForSeconds(0.2f);
        texturePainter.enabled =true;
        if(SaveTexture)
        {
            texturePainter.SaveTexture();
            ClearObst();
        }
        ResetPaintCount();
        texturePainter.uimanager.ResetProgressBar();
        texturePainter.clicked =false;
    }

    public void Clean()
    {
        foreach (Transform child in texturePainter.SensorContainer.transform) {
            Destroy(child.gameObject);
        }
        foreach (Transform sensor in texturePainter.ConnectedLinesContainer.transform) {
            Destroy(sensor.gameObject);                      
        }
        foreach (Transform brush in texturePainter.brushContainer.transform) {
            Destroy(brush.gameObject);                      
        }
    }
    void ClearObst()
    {
        var obs = GameObject.FindGameObjectsWithTag("Obstacle");
        foreach (GameObject obst in obs)
        {
            foreach (Transform brush in obst.transform)
            {
                if(brush.GetComponent<SpriteRenderer>())
                {
                    Destroy(brush.gameObject);
                }
            }
        }
    }

    public void SetCustomLvl()
    {
        texturePainter.uimanager.SetCustomLvL();
        currentlvlUI = currentlvl;
        nextlvlUI = nextLVL;
    }

    public void CalculateNextLvl()
    {
        nextlvlUI = currentlvlUI + 1;
        CountNextLVL = currentlvl + 1;
        if (CountNextLVL < Levels.Count) nextLVL = currentlvl + 1;
        else nextLVL = LvlAfterEnd;

    }

    private void ResetPaintCount()
    {
        paintCount = 0;
    }
    private void CalculatePaintNeed()
    {
        var PaintCount = currentObj.GetComponentsInChildren<EndActiveArea>();

        Levels[currentlvl].paintNeed = PaintCount.Length;
    }

    void changeObstalceColor()
    {
        GameObject[] obs = GameObject.FindGameObjectsWithTag("Obstacle");
        foreach (GameObject obst in obs)
        {
            foreach (Transform brush in obst.transform)
            {
                if(brush.GetComponent<SpriteRenderer>())
                {
                    if(brush.GetComponent<SpriteRenderer>().color == Color.red)
                    {  
                        brush.GetComponent<SpriteRenderer>().color = texturePainter.Red;
                    }
                    if(brush.GetComponent<SpriteRenderer>().color == Color.blue)
                    {   
                        brush.GetComponent<SpriteRenderer>().color = texturePainter.Blue;
                    }
                    Color32 yel = new Color32(255, 255, 0, 255);
                    if(brush.GetComponent<SpriteRenderer>().color == yel)
                    {
                        brush.GetComponent<SpriteRenderer>().color = texturePainter.Yellow;
                    }
                }
            }
        }
    }
}


