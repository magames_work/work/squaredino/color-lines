﻿using System.Collections.Generic;
using UnityEngine;

public class LineInstance : MonoBehaviour
{
    public GameObject currentStart;
    public int currentid;
    private TexturePainter texturePainter;
    public bool isRestart;
    public bool MixEnable;

    bool FixStartGlitch;

    private void Start() {
        
        texturePainter = FindObjectOfType<TexturePainter>();
        var objcs = FindObjectsOfType<SpriteRenderer>();
        foreach(SpriteRenderer sr in objcs)
        {
            sr.material = texturePainter.defaultSpriteMat;
        }
        currentid = 0;
        var startspots = FindObjectsOfType<StartActiveArea>();
        foreach(StartActiveArea sa in startspots)
        {
            foreach(Transform child in sa.transform)
            {
                child.gameObject.layer = 8;
            }
        }
        var endspots = FindObjectsOfType<EndActiveArea>();
        foreach(EndActiveArea sa in endspots)
        {
            foreach(Transform child in sa.transform)
            {
                child.gameObject.layer = 8;
            }
        }

        
    }

    void Update()
    {
		Ray ray = Camera.main.ScreenPointToRay (Input.mousePosition);
        RaycastHit hit;
    
        if(Physics.Raycast(ray, out hit, 200))
        {
            
            if(hit.transform.tag == "back")
            {
                ClearLine();
            }
            if(hit.transform.GetComponentInParent<StartActiveArea>())
            {
                texturePainter.isStart = false;
            }

            if(!texturePainter.FreeDraw)
            {
                if(Input.GetMouseButtonDown(0))
                {
                    try
                    {
                        if(hit.transform.tag == "StartLine")
                        {
                            texturePainter.clicked = true;

                            texturePainter.vibro.TriggerMediumImpact();
                            if(hit.transform.GetComponentInParent<StartActiveArea>().mixid != 0)
                            {
                                if(hit.transform.GetComponentInParent<StartActiveArea>().mixid == 1)
                                {
                                    currentid = 1;
                                    hit.transform.GetComponentInParent<StartActiveArea>().mixid = 1;
                                    texturePainter.catchmousepos = Input.mousePosition;
                                }
                                if(hit.transform.GetComponentInParent<StartActiveArea>().mixid == 2)
                                {
                                    currentid = 2;
                                    hit.transform.GetComponentInParent<StartActiveArea>().mixid = 2;
                                    texturePainter.catchmousepos = Input.mousePosition;
                                }
                                if(hit.transform.GetComponentInParent<StartActiveArea>().mixid == 3)
                                {
                                    currentid = 3;
                                    hit.transform.GetComponentInParent<StartActiveArea>().mixid = 3;
                                    texturePainter.catchmousepos = Input.mousePosition;
                                }
                                
                                if(hit.transform.GetComponentInParent<StartActiveArea>().mixid == 4)
                                {
                                    currentid = 4;
                                    hit.transform.GetComponentInParent<StartActiveArea>().mixid = 4;
                                    MixEnable = true;
                                    texturePainter.startmix = hit.transform.gameObject;
                                    texturePainter.catchmousepos = Input.mousePosition;
                                }
                                if(hit.transform.GetComponentInParent<StartActiveArea>().mixid == 5)
                                {
                                    currentid = 5;
                                    hit.transform.GetComponentInParent<StartActiveArea>().mixid = 5;
                                    MixEnable = true;
                                    texturePainter.startmix = hit.transform.gameObject;
                                    texturePainter.catchmousepos = Input.mousePosition;
                                }
                                if(hit.transform.GetComponentInParent<StartActiveArea>().mixid == 6)
                                {  
                                    currentid = 6;
                                    hit.transform.GetComponentInParent<StartActiveArea>().mixid = 6;
                                    MixEnable = true;
                                    texturePainter.startmix = hit.transform.gameObject;
                                    texturePainter.catchmousepos = Input.mousePosition;
                                }
                            }
                            if(hit.transform.GetComponentInParent<StartActiveArea>().mixid == 0)
                                {
                                    if(hit.transform.GetComponentInParent<StartActiveArea>().startid == 1)
                                    {
                                        currentid = 1;
                                    }
                                    if(hit.transform.GetComponentInParent<StartActiveArea>().startid == 2)
                                    {
                                        currentid = 2;
                                    }
                                    if(hit.transform.GetComponentInParent<StartActiveArea>().startid == 3)
                                    {
                                        currentid = 3;
                                    }
                                    if(hit.transform.GetComponentInParent<StartActiveArea>().startid == 4)
                                    {
                                        currentid = 4;
                                    }
                                    if(hit.transform.GetComponentInParent<StartActiveArea>().startid == 5)
                                    {
                                        currentid = 5;
                                    }
                                    if(hit.transform.GetComponentInParent<StartActiveArea>().startid == 6)
                                    {
                                        currentid = 6;
                                    }
                                }
                            
                            currentStart = hit.transform.gameObject;
                        }
                        if(texturePainter.FreeDraw)
                        {
                            if(texturePainter.color == TexturePainter._Colors.Red)
                            {
                                currentid = 1;
                            }
                            if(texturePainter.color == TexturePainter._Colors.Blue)
                            {
                                currentid = 2;
                            }
                            if(texturePainter.color == TexturePainter._Colors.Yellow)
                            {
                                currentid = 3;
                            }
                            if(texturePainter.color == TexturePainter._Colors.Green)
                            {
                                currentid = 4;
                            }
                            if(texturePainter.color == TexturePainter._Colors.Orange)
                            {
                                currentid = 5;
                            }
                            if(texturePainter.color == TexturePainter._Colors.Violet)
                            {  
                                currentid = 6;
                            }

                            if(Input.GetMouseButtonDown(0))
                                texturePainter.clicked = true;
                        }
                    }   
                    catch{}
                }
            }
            try
            {       
                if(texturePainter.clicked & !texturePainter.FreeDraw)
                {
            //Gray
                    if(currentid == 4 | currentid == 5 | currentid == 6)
                    {
                        if(texturePainter.startmix.GetComponentInParent<StartActiveArea>().isMixed & hit.transform.GetComponentInParent<StartActiveArea>().mixid != currentid)
                        {
                            Gray();
                        }
                    }
                    if(currentid == 1 | currentid == 2 | currentid == 3)
                    {
                        if(hit.transform.GetComponentInParent<StartActiveArea>().mixid >= 4 & hit.transform.GetComponentInParent<StartActiveArea>().mixid != currentid)
                        {
                            Gray();
                        }
                    }

            //Violet (Red + Blue)
                    if(hit.transform.GetComponentInParent<StartActiveArea>().mixid == 1 & currentid == 2)
                    {
                        currentid = 6;
                        MixEnable = true;
                        texturePainter.catchmousepos = Input.mousePosition;
                        texturePainter.startmix = hit.transform.gameObject;
                        hit.transform.GetComponentInParent<SpriteRenderer>().color = texturePainter.Violet;
                        hit.transform.GetComponentInParent<StartActiveArea>().mixid = 6;
                        hit.transform.GetComponentInParent<StartActiveArea>().used = true;
                    }

            //Orange (Red + Yellow)
                    if(hit.transform.GetComponentInParent<StartActiveArea>().mixid == 1 & currentid == 3)
                    {
                        currentid = 5;
                        MixEnable = true;
                        texturePainter.catchmousepos = Input.mousePosition;
                        texturePainter.startmix = hit.transform.gameObject;
                        hit.transform.GetComponentInParent<SpriteRenderer>().color = texturePainter.Orange;
                        hit.transform.GetComponentInParent<StartActiveArea>().mixid = 5;
                        hit.transform.GetComponentInParent<StartActiveArea>().used = true;
                    }

            //Violet (Blue + Red)
                    if(hit.transform.GetComponentInParent<StartActiveArea>().mixid == 2 & currentid == 1)
                    {
                        currentid = 6;
                        MixEnable = true;
                        texturePainter.catchmousepos = Input.mousePosition;
                        texturePainter.startmix = hit.transform.gameObject;
                        hit.transform.GetComponentInParent<SpriteRenderer>().color = texturePainter.Violet;
                        hit.transform.GetComponentInParent<StartActiveArea>().mixid = 6;
                        hit.transform.GetComponentInParent<StartActiveArea>().used = true;
                    }

            //Green (Blue + Yellow)
                    if(hit.transform.GetComponentInParent<StartActiveArea>().mixid == 2 & currentid == 3)
                    {
                        currentid = 4;
                        MixEnable = true;
                        texturePainter.catchmousepos = Input.mousePosition;
                        texturePainter.startmix = hit.transform.gameObject;
                        hit.transform.GetComponentInParent<SpriteRenderer>().color = texturePainter.Green;
                        hit.transform.GetComponentInParent<StartActiveArea>().mixid = 4;
                        hit.transform.GetComponentInParent<StartActiveArea>().used = true;
                    }

            //Orange (Yellow + Red)
                    if(hit.transform.GetComponentInParent<StartActiveArea>().mixid == 3 & currentid == 1)
                    {
                        currentid = 5;
                        MixEnable = true;
                        texturePainter.catchmousepos = Input.mousePosition;
                        texturePainter.startmix = hit.transform.gameObject;
                        hit.transform.GetComponentInParent<SpriteRenderer>().color = texturePainter.Orange;
                        hit.transform.GetComponentInParent<StartActiveArea>().mixid = 5;
                        hit.transform.GetComponentInParent<StartActiveArea>().used = true;
                    }

            //Green (Yellow + Blue)
                    if(hit.transform.GetComponentInParent<StartActiveArea>().mixid == 3 & currentid == 2)
                    {
                        currentid = 4;
                        MixEnable = true;
                        texturePainter.catchmousepos = Input.mousePosition;
                        texturePainter.startmix = hit.transform.gameObject;
                        hit.transform.GetComponentInParent<SpriteRenderer>().color = texturePainter.Green;
                        hit.transform.GetComponentInParent<StartActiveArea>().mixid = 4;
                        hit.transform.GetComponentInParent<StartActiveArea>().used = true;
                    }
                    
                }
            }catch{}
        
            if(hit.transform.tag == "EndLine")
            {
                if(hit.transform.GetComponentInParent<EndActiveArea>().endId == currentid & !hit.transform.GetComponentInParent<EndActiveArea>().complited)
                {
                    if(!hit.transform.GetComponentInParent<EndActiveArea>().complited)
                    {
                        if(hit.transform.gameObject.GetComponent<showtip>())
                        {
                            texturePainter.uimanager.show18();
                        }
                        hit.transform.gameObject.GetComponentInParent<EndActiveArea>().gameObject.AddComponent<StartActiveArea>().EndButStart = true;
                        hit.transform.gameObject.GetComponentInParent<StartActiveArea>().startid = hit.transform.gameObject.GetComponentInParent<EndActiveArea>().endId;
                        hit.transform.gameObject.GetComponentInParent<StartActiveArea>().mixid = hit.transform.gameObject.GetComponentInParent<EndActiveArea>().endId;
                        hit.transform.GetComponent<SphereCollider>().radius = 2;
                        hit.transform.tag = "StartLine";
                        texturePainter.lvlmanager.paintCount++;
                        texturePainter.uimanager.OnPaint();
                        texturePainter.vibro.TriggerHeavyImpact();
                        
                        foreach (Transform child in texturePainter.SensorContainer.transform) {
                            child.transform.tag = "Complited";
                        }
                        var startspots = FindObjectsOfType<StartActiveArea>();
                        foreach(StartActiveArea sa in startspots)
                        {
                            if(sa.used == true)
                            {
                                sa.complited = true;
                            }
                        }
                        
                        SaveTexture();
                        hit.transform.GetComponentInParent<EndActiveArea>().complited = true;

                    }
                    texturePainter.clicked = false;
                    FixStartGlitch = true;
                }
                else if(hit.transform.GetComponentInParent<EndActiveArea>().endId != currentid | hit.transform.GetComponentInParent<EndActiveArea>().complited)
                {
                    //KillChild();
                    ClearLine();
                    texturePainter.clicked = false;
                }
                
            }
            
            if(Input.GetMouseButtonUp(0))
            {
                if(isRestart) {}

                if(FixStartGlitch | isRestart)
                {
                    FixStartGlitch = false;
                }
                else
                {
                    ClearLine();
                }
            }
        }
    }

    public void ClearLine()
    {
        if(!texturePainter.FreeDraw || !isRestart)
        {
            if(texturePainter.clicked)
            {
                texturePainter.clicked = false; 
                texturePainter.isStart = false;
                foreach(Transform brushes in texturePainter.brushContainer.transform) 
                {
                    Destroy(brushes.gameObject);
                }
                foreach(Transform sensors in texturePainter.SensorContainer.transform) 
                {
                    if(sensors.transform.tag != "Complited")
                    {
                        Destroy(sensors.gameObject);
                    }
                }
                var mixedspots = FindObjectsOfType<StartActiveArea>();
                foreach(StartActiveArea sa in mixedspots)
                {
                    if(sa.used == true & !sa.complited)
                    {
                        if(sa.startid == 1)
                        {
                            sa.GetComponent<SpriteRenderer>().color = texturePainter.Red;
                            sa.mixid = 1;
                        }

                        if(sa.startid == 2)
                        {
                            sa.GetComponent<SpriteRenderer>().color = texturePainter.Blue;
                            sa.mixid = 2;
                        }
                        if(sa.startid == 3) 
                        {
                            sa.GetComponent<SpriteRenderer>().color = texturePainter.Yellow;
                            sa.mixid = 3;
                        }
                        sa.used = false;
                    }
                    texturePainter.vibro.TriggerMediumImpact();
                    currentid = 0;
                }
                texturePainter.clicked = false;
            }
        }
        if(texturePainter.FreeDraw)
        {
            texturePainter.isStart = false;
            texturePainter.clicked = false; 
            texturePainter.obstLineCreated = false;
            //print(texturePainter.obstLineCreated);

            
        }
    }

    void SaveTexture()
	{		
        currentid = 0;

		System.DateTime date = System.DateTime.Now;
		RenderTexture.active = texturePainter.canvasTexture;
		Texture2D tex = new Texture2D(texturePainter.canvasTexture.width, texturePainter.canvasTexture.height, TextureFormat.RGB24, false);		
		tex.ReadPixels (new Rect (0, 0, texturePainter.canvasTexture.width, texturePainter.canvasTexture.height), 0, 0);
		tex.Apply ();
		RenderTexture.active = null;
		texturePainter.baseMaterial.mainTexture =tex;
		foreach (Transform child in texturePainter.brushContainer.transform) {
			Destroy(child.gameObject);
		}
        texturePainter.clicked = false;

	}

    void Gray()
    {
        isRestart = true;
        currentid = 10;
        Invoke("Clean", 0.3f);
        texturePainter.vibro.TriggerFailure();
        texturePainter.lvlmanager.Invoke("RestartLvl", 1f);
    }

    void Clean()
    {
        texturePainter.clicked = false;
    }
}
