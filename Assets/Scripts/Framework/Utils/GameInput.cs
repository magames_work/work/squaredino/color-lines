﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace Core.Scripts.Framework.Utils
{
    public class GameInput : MonoBehaviour
    {
        private Image _image;
        public static event Action OnPointerDown = delegate { };
        public static event Action OnPointerUp = delegate { };
        public static event Action OnPointerMove = delegate { };

        private UIManager ui;

        private void Start()
        {
            ui = FindObjectOfType<UIManager>();
            _image = GetComponent<Image>();
            //GlobalEvents<OnGameInputEnable>.Happened += OnGameInputEnable;
        }

        // private void OnGameInputEnable(OnGameInputEnable obj)
        // {
        //     _image.raycastTarget = obj.Flag;
        // }

        public void PointerDown()
        {
            OnPointerDown();
            ui.LvlEnter();
        }

        public void PointerUp()
        {
            OnPointerUp();
        }

        public void PointerMove()
        {
            OnPointerMove();
            ui.LvlEnter();
            
        }
    }
}