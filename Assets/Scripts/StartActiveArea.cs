﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StartActiveArea : MonoBehaviour
{
public enum Colors {Red, Blue, Yellow, Green, Orange, Violet}
    public Colors color;
    private TexturePainter texturePainter;
    private LineInstance instance;
    private VibroManager vibro;
    public bool used;
    public bool complited;
    public bool isMixed;
    public int startid;
    public int mixid;
    public bool EndButStart = false;
    void Start()
    {
        vibro = FindObjectOfType<VibroManager>();
        instance = FindObjectOfType<LineInstance>();
        texturePainter = FindObjectOfType<TexturePainter>();
        GetComponent<SpriteRenderer>().sprite = texturePainter.StartActiveArea;
        gameObject.transform.localScale = Vector3.one * texturePainter.AreaSize;

        if(!EndButStart)
        {
            if(color == Colors.Red) 
            { 
                GetComponent<SpriteRenderer>().color = texturePainter.Red;
                startid = 1;
                mixid = 1;
            }
            if(color == Colors.Blue) 
            { 
                GetComponent<SpriteRenderer>().color = texturePainter.Blue;
                startid = 2;
                mixid = 2;
            }
            if(color == Colors.Yellow)
            {  
                GetComponent<SpriteRenderer>().color = texturePainter.Yellow; 
                startid = 3;
                mixid = 3;
            }
            if(color == Colors.Green) 
            {
                GetComponent<SpriteRenderer>().color = texturePainter.Green; 
                startid = 4;
                mixid = 4;
                isMixed = true;
            }
            if(color == Colors.Orange)  
            {
                GetComponent<SpriteRenderer>().color = texturePainter.Orange;
                startid = 5;
                mixid = 5;
                isMixed = true;
            }
            if(color == Colors.Violet)  
            {
                GetComponent<SpriteRenderer>().color = texturePainter.Violet;
                startid = 6;
                mixid = 6;
                isMixed = true;
            }
        }
    }
}
